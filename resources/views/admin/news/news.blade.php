@extends('layouts.adminlayout');
@section('containt')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">News</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">News</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<div>
	<a href=" {{ route('admin.news.create') }}">
	<button class="btn btn-primary m-2">Add New News</button>
	</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">News List</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="example2" class="table table-bordered  table-hover">
						<thead class="bg-light ">
							<tr>
								<th>No</th>
								<th>Title</th>
								<th>Description</th>
								<th>Author</th>
								<th colspan="2" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($news as $c)
							<tr>
								<td>{{ $c->id }}</td>
								<td>{{ $c->title }} </td>
								<td>{{ $c->description }} </td>
								<td>{{ $c->author }} </td>
								<td><a class="btn btn-primary" href="{{ route('admin.news.edit',$c->id) }}">Edit</a> </td>
								<td>
									<form action="{{ route('admin.news.destroy',$c->id) }}" method="post">
										@csrf
										@method('DELETE')
									<input type="submit" class="btn btn-danger" name="submit" value="Delete">
									</form>
								 </td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection