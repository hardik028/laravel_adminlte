@extends('layouts.adminlayout');
@section('containt')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Add News</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Add News</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>

<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Add News </h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<form action="{{ route('admin.news.store') }}" method="post">
						
						@csrf
						
						<div class="form-group">
							<div class="row">
								<label  class="col-md-3">Title</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="title"  value="{{ old('title') }}" placeholder="Enter Title">	
									@error('title')
									<div class="alert alert-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<label  class="col-md-3">Description</label>
								<div class="col-md-6">
									<textarea class="form-control" name="description" rows="5">
										{{ old('description') }}
									</textarea>

									@error('description')
									<div class="alert alert-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<label  class="col-md-3">Author</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="author"  value="{{ old('author') }}" placeholder="Enter Author">	
									@error('author')
									<div class="alert alert-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="submit"  name='submit'class="btn btn-primary">
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection