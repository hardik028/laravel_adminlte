@extends('layouts.adminlayout');
@section('containt')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Categories</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Categories</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<div>
	<a href=" {{ route('admin.categories.create') }}">
	<button class="btn btn-primary m-2">Add New Categories</button>
	</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Categories List</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="example2" class="table table-bordered  table-hover">
						<thead class="bg-light ">
							<tr>
								<th>No</th>
								<th>Title</th>
								<th colspan="2" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($categories as $c)
							<tr>
								<td>{{ $c->id }}</td>
								<td>{{ $c->title }} </td>
								<td><a class="btn btn-primary" href="{{ route('admin.categories.edit',$c->id) }}">Edit</a> </td>
								<td>{{-- <a class="btn btn-danger" href="javascripat:void(0)" onclick="$(this).parent().find('form').submit()">Delete</a> --}}
									<form action="{{ route('admin.categories.destroy',$c->id) }}" method="post">
										@csrf
										@method('DELETE')
									<input type="submit" class="btn btn-danger" name="submit" value="Delete">			
									</form>
								 </td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection