<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/testhome', 'HomeController@test')->name('testhome');
// Route::get('/home/news', 'CategoriesController@index')->name('News');
// We use resource controller so we commented  
//Route::get('/home/categories', 'CategoriesController@index')->name('listCategories');
// Route::get('/home/categories/detail/{id}', 'CategoriesController@detail')->name('DetailCategory');

Route::resource('/admin/categories','Admin\CategoriesController',['as'=>'admin']);
Route::resource('/admin/news','News\NewsController',['as'=>'admin']);
